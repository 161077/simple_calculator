import 'dart:ffi';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Calculator"),
        ),
        body: CalcWidget(),
      ),
    );
  }
}

class CalcWidget extends StatefulWidget {
  @override
  _CalcState createState() {
    return _CalcState();
  }
}

class _CalcState extends State<CalcWidget> {
  final num1Controller = TextEditingController();
  final num2Controller = TextEditingController();

  int _sum = 0;

  void _add() {
    setState(() {
      _sum = int.parse(num1Controller.text) + int.parse(num2Controller.text);
    });
  }

  void _sub() {
    setState(() {
      _sum = int.parse(num1Controller.text) - int.parse(num2Controller.text);
    });
  }

  void _div() {
    setState(() {
      _sum = (double.parse(num1Controller.text) /
              double.parse(num2Controller.text))
          .round();
    });
  }

  void _mul() {
    setState(() {
      _sum = int.parse(num1Controller.text) * int.parse(num2Controller.text);
    });
  }

  void _clear() {
    setState(() {
      num1Controller.text = "0";
      num2Controller.text = "0";
      _sum = 0;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextField(
          controller: num1Controller,
        ),
        TextField(
          controller: num2Controller,
        ),
        Text('$_sum'),
        FlatButton(
            onPressed: _add, color: Colors.blueAccent, child: Text("Add")),
        FlatButton(
            onPressed: _sub, color: Colors.blueAccent, child: Text("Sub")),
        FlatButton(
            onPressed: _div, color: Colors.blueAccent, child: Text("Div")),
        FlatButton(
            onPressed: _mul, color: Colors.blueAccent, child: Text("Mul")),
        FlatButton(
            onPressed: _clear, color: Colors.redAccent, child: Text("Clear")),
      ],
    );
  }
}
